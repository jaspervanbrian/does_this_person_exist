# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :does_this_person_exist,
  ecto_repos: [DoesThisPersonExist.Repo]

# Configures the endpoint
config :does_this_person_exist, DoesThisPersonExistWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "9gC1NSQy6HVbd66bR2Z4HGiksOQALjIt0NCPJI+sxm4oF8lq0uzRjQSijAhwPRxc",
  render_errors: [view: DoesThisPersonExistWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: DoesThisPersonExist.PubSub,
  live_view: [signing_salt: "7HBxCq9o"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :esbuild,
  version: "0.17.11",
  does_this_person_exist: [
    args:
      ~w(js/app.js --bundle --target=es2017 --outdir=../priv/static/assets --external:/fonts/* --external:/images/*),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

# Configure tailwind (the version is required)
config :tailwind,
  version: "3.4.3",
  does_this_person_exist: [
    args: ~w(
      --config=tailwind.config.js
      --input=css/app.css
      --output=../priv/static/assets/app.css
    ),
    cd: Path.expand("../assets", __DIR__)
  ]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
