defmodule DoesThisPersonExist.GamesTest do
  use DoesThisPersonExist.DataCase

  alias DoesThisPersonExist.Games

  describe "games" do
    alias DoesThisPersonExist.Games.{
      Game,
      Round
    }

    @user_valid_attrs %{username: "some username"}
    @user_update_attrs %{username: "some updated username"}
    @user_invalid_attrs %{username: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@user_valid_attrs)
        |> Accounts.create_user()

      user
    end

    @valid_attrs %{remaining_seconds: 42, score: 42, consecutive_corrects: 0}
    @update_attrs %{remaining_seconds: 43, score: 43, consecutive_corrects: 0}
    @invalid_attrs %{remaining_seconds: nil, score: nil, consecutive_corrects: nil}

    @active_round_valid_attrs %{active: true, correct: false, image_filename: "sample.jpg"}

    def game_fixture(%User{} = user, attrs \\ %{}) do
      {:ok, game} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Map.put(:user_id, user.id)
        |> Games.create_game()

      game
    end

    def round_fixture(%Game{} = game, attrs \\ %{}) do
      {:ok, round} =
        attrs
        |> Enum.into(@active_round_valid_attrs)
        |> Map.put(:game_id, game.id)
        |> Games.create_round()

      round
    end

    test "list_games/0 returns all games" do
      game = game_fixture()
      assert Games.list_games() == [game]
    end

    test "get_game!/1 returns the game with given id" do
      game = game_fixture()
      assert Games.get_game!(game.id) == game
    end

    test "create_game/1 with valid data creates a game" do
      assert {:ok, %Game{} = game} = Games.create_game(@valid_attrs)
      assert game.remaining_seconds == 42
      assert game.score == 42
    end

    test "create_game/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Games.create_game(@invalid_attrs)
    end

    test "update_game/2 with valid data updates the game" do
      game = game_fixture()
      assert {:ok, %Game{} = game} = Games.update_game(game, @update_attrs)
      assert game.remaining_seconds == 43
      assert game.score == 43
    end

    test "update_game/2 with invalid data returns error changeset" do
      game = game_fixture()
      assert {:error, %Ecto.Changeset{}} = Games.update_game(game, @invalid_attrs)
      assert game == Games.get_game!(game.id)
    end

    test "delete_game/1 deletes the game" do
      game = game_fixture()
      assert {:ok, %Game{}} = Games.delete_game(game)
      assert_raise Ecto.NoResultsError, fn -> Games.get_game!(game.id) end
    end

    test "change_game/1 returns a game changeset" do
      game = game_fixture()
      assert %Ecto.Changeset{} = Games.change_game(game)
    end

    test "guess_round/2 returns round with correct set to false if category mismatch" do
      user = user_fixture()
      game = game_fixture()
      round = round_fixture(game, %{category: :does_exist})
      category_answer = :does_not_exist

      assert {:ok, %Round{} = round} = Games.guess_round(%{round: round, game: game, category_answer: category_answer})
      assert round.correct == false
    end
  end
end
