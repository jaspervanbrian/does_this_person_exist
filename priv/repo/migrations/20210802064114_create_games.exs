defmodule DoesThisPersonExist.Repo.Migrations.CreateGames do
  use Ecto.Migration

  def change do
    create table(:games) do
      add :remaining_seconds, :integer, default: 60, null: false
      add :score, :integer, default: 0, null: false
      add :user_id, references(:users, on_delete: :delete_all), null: false

      timestamps()
    end

    create index(:games, [:user_id])
  end
end
