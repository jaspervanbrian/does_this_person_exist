defmodule DoesThisPersonExist.Repo.Migrations.CreateRounds do
  use Ecto.Migration

  alias DoesThisPersonExist.RoundCategoryEnum

  def change do
    RoundCategoryEnum.create_type()

    create table(:rounds) do
      add :image_filename, :string, null: false
      add :active, :boolean, default: true, null: false
      add :correct, :boolean, default: false, null: false
      add :game_id, references(:games, on_delete: :delete_all), null: false
      add :category, RoundCategoryEnum.type(), null: false

      timestamps()
    end

    create index(:rounds, [:game_id])
  end
end
