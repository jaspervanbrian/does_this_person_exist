defmodule DoesThisPersonExist.Repo.Migrations.AddConsecutiveCorrectsToGame do
  use Ecto.Migration

  def change do
    alter table("games") do
      add :consecutive_corrects, :integer, default: 0
    end
  end
end
