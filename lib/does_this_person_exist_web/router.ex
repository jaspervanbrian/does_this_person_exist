defmodule DoesThisPersonExistWeb.Router do
  use DoesThisPersonExistWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {DoesThisPersonExistWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :protected do
    plug DoesThisPersonExist.Plugs.GatekeeperPlug
  end

  scope "/", DoesThisPersonExistWeb do
    pipe_through :browser

    get "/", SessionController, :index
    post "/", SessionController, :create
  end

  scope "/game", DoesThisPersonExistWeb.Game do
    pipe_through :browser
    pipe_through :protected

    live "/", GameLive, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", DoesThisPersonExistWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: DoesThisPersonExistWeb.Telemetry
    end
  end
end
