defmodule DoesThisPersonExistWeb.SessionController do
  use DoesThisPersonExistWeb, :controller

  alias DoesThisPersonExist.{
    Accounts,
    Repo
  }

  def index(conn, _) do
    user = get_session(conn, :current_user)
    username = if user, do: user.username, else: ''

    render(conn, "index.html", username: username)
  end

  def create(conn, %{"username" => username}) do
    user =
      Accounts.User
      |> Repo.get_by(username: username)
      |> case do
        nil ->
          {:ok, user} = Accounts.create_user(%{username: username})
          user

        %Accounts.User{} = user -> user
      end

    conn
    |> Plug.Conn.put_session(:current_user, user)
    |> redirect(to: Routes.game_path(conn, :index))
  end
end
