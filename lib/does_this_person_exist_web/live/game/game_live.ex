defmodule DoesThisPersonExistWeb.Game.GameLive do
  use DoesThisPersonExistWeb, :live_view

  alias DoesThisPersonExist.{
    Monitors,
    Games
  }

  """
    Game Statuses:
    - playing
    - paused
    - in_game_in_another_session
    - game_over
  """

  @impl true
  def mount(_params, %{"current_user" => user}, socket) do
    active_game = Games.fetch_active_game(user)
    last_round = if active_game, do: Games.fetch_last_round(active_game)
    remaining_seconds = if active_game, do: active_game.remaining_seconds, else: 60
    score = if active_game, do: active_game.score, else: 0
    consecutive_corrects = if active_game, do: active_game.consecutive_corrects, else: 0
    high_score = Games.get_high_score(user) || 0

    status =
      if active_game do
        Monitors.GameMonitor.monitor(%{pid: self(), game: active_game})
        |> case do
          :ok -> :paused

          :in_game -> :in_game_in_another_session
        end
      else
        :playing
      end

    {:ok, assign(socket,
      current_user: user,
      active_game: active_game,
      round: last_round,
      status: status,
      score: score,
      high_score: high_score,
      remaining_seconds: remaining_seconds,
      consecutive_corrects: consecutive_corrects,
    )}
  end

  @impl true
  def handle_event("start_new_game", _, %{assigns: assigns} = socket) do
    {:ok, active_game} = Games.create_game(%{
      user_id: assigns.current_user.id,
      remaining_seconds: 60,
      score: 0,
      consecutive_corrects: 0,
    })

    {:ok, active_round} = create_round(active_game)

    status =
      with :ok <- Monitors.GameMonitor.monitor(%{pid: self(), game: active_game}),
           :ok <- Monitors.GameMonitor.start_game(active_game.id)
      do
        :playing
      else
        _ -> :in_game_in_another_session
      end

    {:noreply, assign(socket,
      active_game: active_game,
      round: active_round,
      status: status,
    )}
  end

  @impl true
  def handle_event("continue_game", _, %{assigns: assigns} = socket) do
    %{
      active_game: active_game,
      round: round
    } = assigns

    status =
      if round.active do
        with :ok <- Monitors.GameMonitor.start_game(active_game.id) do
          :playing
        else
          _ -> :in_game_in_another_session
        end
      else
        :playing
      end

    {:noreply, assign(socket,
      status: status,
    )}
  end

  @impl true
  def handle_event("guess", %{"category" => category_answer}, %{assigns: assigns} = socket) do
    %{active_game: active_game, round: round} = assigns

    {:ok, round} =
      %{game: active_game, round: round, category_answer: category_answer}
      |> guess_round()

    {:noreply, assign(socket,
      round: round,
    )}
  end

  @impl true
  def handle_event("next_round", _, %{assigns: assigns} = socket) do
    %{active_game: active_game} = assigns

    {:ok, active_round} = create_round(active_game)

    :ok = Monitors.GameMonitor.start_game(active_game.id)

    {:noreply, assign(socket,
      round: active_round,
    )}
  end

  @impl true
  def handle_info({:correct_guess, %{score: score, consecutive_corrects: consecutive_corrects}}, %{assigns: assigns} = socket) do
    high_score = if assigns.high_score < score, do: score, else: assigns.high_score

    {:noreply, assign(socket, score: score, high_score: high_score, consecutive_corrects: consecutive_corrects)}
  end

  @impl true
  def handle_info(:reset_consecutive_corrects, %{assigns: assigns} = socket) do
    {:noreply, assign(socket, consecutive_corrects: 0)}
  end

  @impl true
  def handle_info({:tick, remaining_seconds}, socket) do
    {:noreply, assign(socket, remaining_seconds: remaining_seconds)}
  end

  @impl true
  def handle_info({:game_over, score}, %{assigns: assigns} = socket) do
    %{active_game: active_game, round: round} = assigns

    {:ok, round} =
      Games.update_round(round, %{
        correct: false,
        active: false,
      })

    {:ok, game} =
      active_game
      |> Games.update_game(%{
        remaining_seconds: 0,
        score: score
      })

    {:noreply, assign(socket,
      active_game: game,
      round: round,
      status: :game_over,
    )}
  end

  defp generate_exists_id, do: Enum.random(0..69_999) |> Integer.to_string |> String.pad_leading(5, "0")

  defp create_round(%Games.Game{id: game_id}) do
    category = Enum.random([:does_exist, :does_not_exist])

    image_url = case category do
      :does_exist ->
        "https://whichfaceisreal.blob.core.windows.net/public/realimages/#{generate_exists_id()}.jpeg"

      :does_not_exist -> "https://thispersondoesnotexist.com"
    end

    %HTTPoison.Response{body: body} = HTTPoison.get!(image_url)

    image_id = Ecto.UUID.generate()
    image_filename = "#{image_id}.jpg"

    spawn(fn ->
      File.mkdir_p!("photos")
      File.write!("photos/#{image_filename}", body)
    end)

    Games.create_round(%{
      game_id: game_id,
      active: true,
      correct: false,
      image_filename: image_filename,
      category: category
    })
  end

  defp guess_round(%{round: round, game: game, category_answer: category_answer}) do
    correct = Atom.to_string(round.category) === category_answer

    if correct do
      Monitors.GameMonitor.correct_guess(game.id)
    else
      Monitors.GameMonitor.pause_game(game.id)
    end

    Games.update_round(round, %{
      correct: correct,
      active: false,
    })
  end
end
