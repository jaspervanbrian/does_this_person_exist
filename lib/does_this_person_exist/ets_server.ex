defmodule DoesThisPersonExist.EtsServer do
  use GenServer

  @tables ~w(
    game_remaining_time game_score game_consecutive_corrects
  )a

  def init(arg) do
    Enum.each(@tables, &:ets.new(&1, [:set, :public, :named_table]))
    {:ok, arg}
  end

  @doc """
  Returns the value for the given ETS table and key. Creates the table if it does
  not exist.

  If a default is provided, this value will be stored in the ETS table and returned.

  ## Examples

      lookup(:shopify_nonces, "myshop.myshopify.com")
      #=> nil

      lookup(:shopify_nonces, "myshop.myshopify.com", SecureRandom.urlsafe_base64())
      #=> "S0xVOUg4a2hEOStGT25kZFlUa0xXZz09"
  """
  def lookup(table_name, key) when table_name in @tables do
    case :ets.lookup(table_name, key) do
      [{^key, value}] -> value
      [] -> nil
    end
  end

  def lookup(table_name, key, default) when table_name in @tables do
    case :ets.lookup(table_name, key) do
      [{^key, value}] ->
        value

      [] ->
        insert(table_name, key, default)
        default
    end
  end

  @doc """
  Inserts the value into the ETS tables under the given key.

  ## Examples

      insert(:shopify_nonces, "myshop.myshopify.com", SecureRandom.urlsafe_base64())
      #=> true
  """
  def insert(table_name, key, value) when table_name in @tables do
    :ets.insert(table_name, {key, value})
  end

  @doc """
  Inserts the value into the ETS tables under the given key.

  ## Examples

      insert_into_table(:shopify_nonces, "myshop.myshopify.com", SecureRandom.urlsafe_base64())
      #=> true
  """
  def delete(table_name, key) when table_name in @tables do
    :ets.delete(table_name, key)
  end

  def start_link(arg) do
    GenServer.start_link(__MODULE__, arg, name: __MODULE__)
  end
end
