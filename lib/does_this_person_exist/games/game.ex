defmodule DoesThisPersonExist.Games.Game do
  use Ecto.Schema
  import Ecto.Changeset

  alias DoesThisPersonExist.Accounts.User
  alias DoesThisPersonExist.Games.Round

  schema "games" do
    field :remaining_seconds, :integer
    field :score, :integer
    field :consecutive_corrects, :integer

    belongs_to :user, User

    has_many :rounds, Round

    timestamps()
  end

  @doc false
  def changeset(game, attrs) do
    game
    |> cast(attrs, [
      :remaining_seconds,
      :consecutive_corrects,
      :score,
      :user_id
    ])
    |> validate_required([
      :remaining_seconds,
      :consecutive_corrects,
      :score,
      :user_id
    ])
  end
end
