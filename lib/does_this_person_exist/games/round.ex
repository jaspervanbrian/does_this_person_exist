defmodule DoesThisPersonExist.Games.Round do
  use Ecto.Schema
  import Ecto.Changeset

  alias DoesThisPersonExist.RoundCategoryEnum
  alias DoesThisPersonExist.Games.Game

  schema "rounds" do
    field :active, :boolean, default: true
    field :correct, :boolean, default: false
    field :image_filename, :string
    field :category, RoundCategoryEnum

    belongs_to :game, Game

    timestamps()
  end

  @doc false
  def changeset(round, attrs) do
    round
    |> cast(attrs, [
      :image_filename,
      :correct,
      :active,
      :category,
      :game_id
    ])
    |> validate_required([
      :image_filename,
      :correct,
      :active,
      :category,
      :game_id
    ])
  end
end
