import EctoEnum

defenum(DoesThisPersonExist.RoundCategoryEnum, :round_category, [
  :does_exist,
  :does_not_exist
])
