defmodule DoesThisPersonExist.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      DoesThisPersonExist.Repo,
      # Start the Telemetry supervisor
      DoesThisPersonExistWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: DoesThisPersonExist.PubSub},
      # Start the Endpoint (http/https)
      DoesThisPersonExistWeb.Endpoint,
      # Start a worker by calling: DoesThisPersonExist.Worker.start_link(arg)
      # {DoesThisPersonExist.Worker, arg}
      DoesThisPersonExist.Monitors.GameMonitor,
      DoesThisPersonExist.EtsServer
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: DoesThisPersonExist.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    DoesThisPersonExistWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
