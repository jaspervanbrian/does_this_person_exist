defmodule DoesThisPersonExist.Monitors.GameMonitor do
  use GenServer

  alias DoesThisPersonExist.{
    EtsServer,
    Games
  }

  def start_link(default) do
    GenServer.start_link(__MODULE__, default, name: __MODULE__)
  end

  def monitor(%{pid: pid, game: game}) do
    GenServer.call(__MODULE__, {:monitor, %{pid: pid, game: game}})
  end

  def start_game(game_id) do
    GenServer.call(__MODULE__, {:start_game, game_id})
  end

  def pause_game(game_id) do
    GenServer.call(__MODULE__, {:pause_game, game_id})
  end

  def correct_guess(game_id) do
    GenServer.call(__MODULE__, {:correct_guess_and_pause, game_id})
  end

  @impl true
  def init(_) do
    {:ok, %{}}
  end

  @impl true
  def handle_call({:monitor, %{pid: pid, game: game}}, _, state) do
    state[game.id]
    |> case do
      nil ->
        Process.monitor(pid)

        new_state = Map.put(state, game.id, %{pid: pid, timer: nil})

        EtsServer.insert(:game_remaining_time, game.id, game.remaining_seconds)
        EtsServer.insert(:game_consecutive_corrects, game.id, game.consecutive_corrects)
        EtsServer.insert(:game_score, game.id, game.score)

        {:reply, :ok, new_state}

      _ -> {:reply, :in_game, state}
    end
  end

  @impl true
  def handle_call({:start_game, game_id}, _, state) do
    state[game_id]
    |> case do
      nil -> {:reply, :not_found, state}

      %{pid: pid, timer: _timer} ->
        timer_ref = progress_timer(game_id)
        new_state = Map.put(state, game_id, %{pid: pid, timer: timer_ref})

        {:reply, :ok, new_state}
    end
  end

  @impl true
  def handle_call({:correct_guess_and_pause, game_id}, _, state) do
    state[game_id]
    |> case do
      nil -> {:reply, :not_found, state}

      %{pid: pid, timer: timer} ->
        if is_reference(timer) do
          Process.cancel_timer(timer)
        end

        score = EtsServer.lookup(:game_score, game_id)
        score = score + 1
        EtsServer.insert(:game_score, game_id, score)

        consecutive_corrects = EtsServer.lookup(:game_consecutive_corrects, game_id)
        consecutive_corrects = consecutive_corrects + 1
        EtsServer.insert(:game_consecutive_corrects, game_id, consecutive_corrects)

        if consecutive_corrects > 2 do
          game_remaining_time = EtsServer.lookup(:game_remaining_time, game_id)
          game_remaining_time = game_remaining_time + 5
          EtsServer.insert(:game_remaining_time, game_id, game_remaining_time)
        end

        :ok = Process.send(pid, {:correct_guess, %{score: score, consecutive_corrects: consecutive_corrects}}, [])

        new_state = Map.put(state, game_id, %{pid: pid, timer: :paused})

        {:reply, :ok, new_state}
    end
  end

  @impl true
  def handle_call({:pause_game, game_id}, _, state) do
    state[game_id]
    |> case do
      nil -> {:reply, :not_found, state}

      %{pid: pid, timer: timer} ->
        case timer do
          :paused ->
            {:reply, :ok, state}

          timer ->
            if is_reference(timer) do
              Process.cancel_timer(timer)
            end

            EtsServer.insert(:game_consecutive_corrects, game_id, 0)
            :ok = Process.send(pid, :reset_consecutive_corrects, [])

            new_state = Map.put(state, game_id, %{pid: pid, timer: :paused})

            {:reply, :ok, new_state}
        end
    end
  end

  @impl true
  def handle_info({:progress_timer, game_id}, state) do
    state[game_id]
    |> case do
      nil -> {:reply, :not_found, state}

      %{pid: pid, timer: timer} ->
        if timer !== :paused do
          if is_reference(timer) do
            Process.cancel_timer(timer)
          end

          remaining_seconds = EtsServer.lookup(:game_remaining_time, game_id)

          if remaining_seconds > 0 do
            remaining_seconds = remaining_seconds - 1

            EtsServer.insert(:game_remaining_time, game_id, remaining_seconds)

            :ok = Process.send(pid, {:tick, remaining_seconds}, [])

            timer_ref = progress_timer(game_id)
            new_state = Map.put(state, game_id, %{pid: pid, timer: timer_ref})

            {:noreply, new_state}
          else
            score = EtsServer.lookup(:game_score, game_id)

            :ok = Process.send(pid, {:game_over, score}, [])

            EtsServer.delete(:game_remaining_time, game_id)
            EtsServer.delete(:game_score, game_id)

            new_state = Map.delete(state, game_id)

            {:noreply, new_state}
          end
        else
          {:noreply, state}
        end
    end
  end

  @impl true
  def handle_info({:DOWN, _ref, :process, pid, _reason}, state) do
    IO.inspect(String.duplicate("=", 200))
    IO.inspect(pid)
    IO.inspect(String.duplicate("=", 200))

    game =
      state
      |> Enum.find(fn {_, val} -> val[:pid] == pid end)

    case game do
      {game_id, %{pid: game_pid, timer: timer}} ->
        if game_pid === pid && is_reference(timer) do
          Process.cancel_timer(timer)
        end

        remaining_seconds = EtsServer.lookup(:game_remaining_time, game_id)
        score = EtsServer.lookup(:game_score, game_id)

        Games.get_game!(game_id)
        |> Games.update_game(%{
          remaining_seconds: remaining_seconds,
          score: score
        })

        EtsServer.delete(:game_remaining_time, game_id)
        EtsServer.delete(:game_score, game_id)

        new_state = Map.delete(state, game_id)

        {:noreply, new_state}

      _ -> {:noreply, state}
    end
  end

  defp progress_timer(game_id) do
    Process.send_after(self(), {:progress_timer, game_id}, 1000)
  end
end
