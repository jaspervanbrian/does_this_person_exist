defmodule DoesThisPersonExist.Plugs.GatekeeperPlug do
  alias DoesThisPersonExistWeb.Router.Helpers, as: Routes

  def init(options), do: options

  def call(conn, _) do
    case conn |> Plug.Conn.get_session(:current_user) do
      nil ->
        conn
        |> Phoenix.Controller.redirect(to: Routes.session_path(conn, :index))
        |> Plug.Conn.halt()

      _ ->
        conn
    end
  end
end
