defmodule DoesThisPersonExist.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  alias DoesThisPersonExist.Games.Game

  schema "users" do
    field :username, :string

    has_many :games, Game

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [
      :username,
    ])
    |> validate_required([:username])
    |> unique_constraint(:username)
  end
end
