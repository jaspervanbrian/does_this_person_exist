defmodule DoesThisPersonExist.Repo do
  use Ecto.Repo,
    otp_app: :does_this_person_exist,
    adapter: Ecto.Adapters.Postgres
end
